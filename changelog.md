# **Changelog**
Changelog per il progetto **FabGuard**. Tutte le maggiori modifiche fatte nel corso del tempo al progetto saranno segnate qui.

Il formato di questo documento è basato su [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

Il progetto aderisce al formato [Semantic Versioning](https://semver.org/spec/v2.0.0.html)
## [v0.1.0] - 15/10/2018
Soluzioni per i bug hardware / meccanici riscontrati

### **Added**

## **Changed**

## **Fixed**

## [v0.0.0]
Inizio del progetto

### **Added**
Aggiunti tutti i file per le parti di:
* Design: disegnato e stampato in 3D il case per alloggiare l'elettronica composta da
    * Raspberry Pi 3
    * MFRC522 
    * Sensore PIR
* hardware: progettazione e realizzazione schema. Creazione pcb tramite CNC
* firmware: realizzazione in linguaggio C di:
    * driver comunicazione tra raspi e mfrc522
    * logica di funzionamento del programma: attivazione del timer alla rilevazione di movimento da parte del PIR sensor e wait per la lettura scheda fino allo scadere del timeout. Visualizzazione stato abbonamento o allarme in caso di mancata visualizzazione
    * Lettura e scrittura in file .csv dello stato degli abbonamenti utenti
### **Changed**
### **Removed**
### **Fixed**
### **Deprecated**
* Design:
    * Ridisposizione componenti per creare un case più compatto
    * assotigliamento parte mfrc522 per diminuire schermatura
    * creazione dei task per poter inserire USB e cavo di rete sul raspi
    * Creazione spazio per alloggiamento futuro di batteria + circuito di controllo
* Hardware:
    * Rimozione sensore PIR
    * Aggiunta Webcam per controllo
    * Aggiunta LED + fotoresistenza per illuminazione notturna
* Firmware: 
    * Riscrittura totale del programma in linguaggio Python
    * Salvataggio e lettura dati direttamente da Google Drive
    * OpenCV per la gestione controllo stanza tramite webcam
    * creazione GUI 
    * Possibilità di aggiungere / rimuovere abbonamenti direttamente da parte degli amministratori

