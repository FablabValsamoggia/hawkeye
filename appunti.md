# Appunti

## 15/10/2018
inizio della versione v1.0.0 (aka FabGuard) del controllo accessi. Come descritto nel changelog del progetto, le cose da fare sono:
* Design:
    * Ridisposizione componenti per creare un case più compatto
    * assotigliamento parte mfrc522 per diminuire schermatura
    * creazione dei task per poter inserire USB e cavo di rete sul raspi
    * Creazione spazio per alloggiamento futuro di batteria + circuito di controllo
* Hardware:
    * Rimozione sensore PIR
    * Aggiunta Webcam per controllo
    * Aggiunta LED + fotoresistenza per illuminazione notturna
* Firmware: 
    * Riscrittura totale del programma in linguaggio Python
    * Salvataggio e lettura dati direttamente da Google Drive
    * OpenCV per la gestione controllo stanza tramite webcam
    * creazione GUI 
    * Possibilità di aggiungere / rimuovere abbonamenti direttamente da parte degli amministratori

Partiamo dal software. Devo prima capire se è possibile implementare un controllo dei movimenti in maniera efficace utilizzando la combo pi + picamera. In rete c'è un sacco di materiale, ma è sempre meglio fare un test per verificare che il tutto sia realmente fattibile.

Cosa ci serve:
* Un raspberry - meglio il 3 ma dovrebbe andare bene anche il 2
* SD
* pi camera: ho comprato [questa](https://www.amazon.it/gp/product/B01M6UCEM5/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1). Funziona bene, le foto e i video sono accettabili con una buona fonte luminosa.

### inizializzazione Raspberry
In rete ci sono milioni di guide (letteralmente) su come scaricare e installare Raspian su SD. Parto quindi dal presupposto che il Raspberry sia già acceso, con il sistema operativo funzionante e con ssh abilitato. Se così non fosse, cercate in rete :satisfied:
1. ssh con X server `ssh -X pi@ip-rasperry`: permette di visualizzare anche le finestre
2. `sudo raspi-config`: in *advance options* abilitiamo
    1. expand filesystem: questo permetterà al nostro pi di utilizzare tutta la dimensione della scheda SD (ci servirà un sacco di spazio quando installeremo OpenCv, consiglio almeno una Sd da 16Gb di quelle performanti)
3. (**Opzionale**) Non avendo a portata di mano lo schermo touch screen, ho deciso di installare anche un vnc server. Non è un passaggio prettamente essenziale ma è comodo avere lo schermo del pi sul proprio pc a volte. Ho scelto tightvnc per tutta una serie di ragioni: è leggero, pronto all'uso e si richiama facilmente da riga di comando.
    1. Lo installiamo con il comando `sudo apt-get install tightvncserver`
    2. Ci verrà chiesta una password di 8 caratteri: io ho scelto raspberr . *Nota bene*: essendo in una fase di test, possiamo anche infischiarcene della sicurezza password. Nel caso in cui l'oggetto debba finire in una zona "sensibile", raccomando l'utilizzo di password strong :)
    3. Per avviare il vnc server ci basterà digitare `vncserver :1 -geometry 1280x720 -depth 24` rimando al manuale di vncserver per i dettagli del comando
    4. Dal terminale del nostro pc (quindi fuori dal raspberry) per attivare lo schermo remoto
        1. `sudo apt-get install xtightvncviewer`
        2. `xtightvncviewer&`: ci verrà richiesto ip del raspberry e la password. N.B. nel digitare ip del raspberry, terminare con :1 (es. 192.168.1.6:1): questo perchè abbiamo abilitato lo schermo remoto sulla "finestra 1"

Passiamo a Python. Dovrebbe già essere installato nel nostro sistema, per verificare se python 3 è correttamente funzionante basterà digitare su terminale python3. Se il comando è riconosciuto, si avrà una schermata come questa

![immagine_python](docs/pictures/schermata_python3.png)

Perfetto, ora la parte più ostica: OpenCV

## 21/10/2018
Per installare OpenCV sul nostro Raspberry abbiamo bisogno di due cose fondamentali:
* Spazio di archiviazione: suggerisco una SD di (almeno) 16Gb. In extremis, è possibile installare o Raspbian versione base, o disinstallare alcuni componenti come `Wolfram-engine` e `libreoffice` tramite i comandi
  ```BASH
  sudo apt-get purge wolfram-engine
  sudo apt-get purge libreoffice*
  sudo apt-get clean
  sudo apt-get autoremove
  ```
  In questo modo si recupera 1Gb di spazio circa
* Pazienza: OpenCV è un software potente e voluminoso, e necessita di risorse per poterlo compilare e far funzionare correttamente. Sul Raspberry quindi ci vorranno un paio d'ore prima di essere operativi

Detto questo, vediamo i passi successivi.

### Dipendenze
Come prima cosa vediamo se il nostro software è aggiornato all'ultima versione con il classico comando
```sh
$ sudo apt-get upgrade && sudo apt-get update
```
Installimao i tool di sviluppo necessari
```sh
$ sudo apt-get install build-essential cmake pkg-config
```
Per chi non conoscessi cmake, rimando al [sito](https://cmake.org/). Potremmo dedicare ore solo a questo argomento.

Alcuni pacchetti per la gestione I/O delle immagini

```sh
$ sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
```
Ed anche un paio di pacchetti per la gestione dell'I/O video, il supporto ai vari formati e lo streaming da webcam
```sh
$ sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
$ sudo apt-get install libxvidcore-dev libx264-dev
```
OpenCV ha implemementata una mini GUI per visualizzare immagini / video a schermo. Per poterla utilizzare serve il modulo highgui che a sua volta necessita delle librerie GTK per funzionare. Le installiamo col comando
```sh
$ sudo apt-get install libgtk2.0-dev libgtk-3-dev
```
Alcune delle funzioni più complesse di OpenCV (come le matrix operation) possono essere ottimizzate con alcune extre dipendenze, fondamentale per funzionare bene in un hardware limitato come il  pi
```sh
$ sudo apt-get install libatlas-base-dev gfortran
```
Installiamo infine gli header di Python 2 e 3 che ci permetteranno di compilare OpenCV interfacciato con Python
```sh
$ sudo apt-get install python2.7-dev python3-dev
```
**Piccola nota** OpenCV, è un software scritto e compilato in C++, per tutta una serie di ragioni che non sto qui ad elencare. Ovviamente un programma scritto in C++ che utilizza le librerie e le funzioni di OpenCV è molto più veloce dello stesso programma scritto in Python. Il grande contro però rimane la complessità maggiore del C++ e la pena di fare un cross compiling decente su Raspberry. Ad oggi, non ho ancora trovato una guida, tutorial o santone che permetta di fare una compilazione comoda da pc in C / C++ per programmi da mettere su Raspberry

## Scarichiamo OpenCV
Scarichiamo la full version di OpenCV in una cartella di nostro piacimento
```sh
$ cd ~
$ wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.3.0.zip
$ unzip opencv.zip
```
Siccome vogliamo la FULL VERSION, scarichiamo anche `opencv-contrib`

**NOTA** assicurarsi sempre che le versioni di `opencv` e `opencv-contrib` siano le stesse altrimenti è un casino compilarle assieme  :laughing:
```sh
$ wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.3.0.zip
$ unzip opencv_contrib.zip
```
### La scelta di Python
Siamo ora giunti ad uno spartiacque: quale versione di Python usare?? Dopo una breve riflessione, ho deciso di optare per Python3, che è ormai lo standard. Comunque il procedimento qui elencato dovrebbe funzionare anche per Python 2.

Bene, appurata la versione, installiamo un paio di utility che ci saranno comode. Per prima cosa `pip`
```sh
$ wget https://bootstrap.pypa.io/get-pip.py
$ sudo python3 get-pip.py
```

### Ambiente virtuale
Partiamo da un presupposto: Python è un gran linguaggio di programmazione, semplice, veloce ed intuitivo. Ma è un gran casino per quanto riguardo i pacchetti e le varie dipendenze. Un programma può installare pacchetti e dipendenze che potrebbero andare a romperne un altro scatenando emicranie al pover programmatore che cerca di capire il problema. Per evitarlo, dopo varie ricerche, mi sono imbattuto in un paio di tool preziosissimi: [virtualenv](https://virtualenv.pypa.io/en/stable/) e [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/). In breve, permettono di creare degli ambienti Python completamente isolati e indipendenti gli uni dagli altri. Fantastico  :heart_eyes: . Ora, non è obbligatorio installarli, OpenCV funziona lo stesso, ma se volete evitare casini futuri **è caldamente consigliato**. Per installarli
```sh
$ sudo pip install virtualenv virtualenvwrapper
$ sudo rm -rf ~/.cache/pip
```

Ora dobbiamo eseguire l'upload del profilo per caricare correttamente i pacchetti. Tramite nano, vim o qualsiasi altro text editor aggiungiamo al file  `~/.profile`
```sh
# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh
```
Per ricaricare il profilo in runtime
```sh
source ~/.profile
```
Creiamo il nostro ambiente isolato; lo chiameremo cv
```sh
mkvirtualenv cv -p python3
```

Per entrare nell'ambiente basta digitare
```sh
$ source ~/.profile
$ workon cv
```
[//]: # (inserire immagine per cv)

### Un ultimo sforzo
Ora che siamo nell'ambiente cv, installiamo il pacchetto `NumPy`, usato per il numerical processing da Python
```sh
pip install numpy
```

**NOTA** potrebbe volerci un po'. Il fatto che non compaia una barra di caricamento o una qualche stringa di testo non significa che il nostro pi si è impallato ma solo che sta installando il nostro pacchetto

### Installiamo OpenCV
Finalmente siamo pronti ad installare OpenCV  :muscle:. Per chi non ha mai usato CMAKE e/o non ha mai programmato in C, questa parte sarà un po' incomprensibile. Creiamo il nostro makefile con i comandi
```sh
$ cd ~/opencv-3.3.0/
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.3.0/modules \
    -D BUILD_EXAMPLES=ON ..
```
Se tutto è andato a buon fine. Siamo finalmente pronti a compilare. Per spremere al massimo il nostro Pi, utilizzando tutti e 4 i core riducendo così i tempi di compilazione, conviene modificare il file di swap in `/etc/dphys-swapfile` modificando in questo modo
```sh
# set size to absolute value, leaving empty (default) then uses computed value
#   you most likely don't want this, unless you have an special disk situation
# CONF_SWAPSIZE=100
CONF_SWAPSIZE=1024
```
Ricarichiamo le info coi comandi
```sh
$ sudo /etc/init.d/dphys-swapfile stop
$ sudo /etc/init.d/dphys-swapfile start
```

**NOTA** Ricordarsi, terminata l'installazione, di ripristinare il valore iniziale altrimenti la nostra SD avrà vita breve.

Bene, ora compiliamo con il comando
```sh
$ make -j4
```

Mettevi comodi, leggetevi un libro o fate quello che volete. Il processo durerà almeno un'ora e mezza.

Se non ci sono stati errori, possiamo finalmente installare OpenCV con i comandi
```sh 
$ sudo make install
$ sudo ldconfig
```
Controlliamo che sia effettivamente tutto al posto giusto. Se con il comando `ls -l /usr/local/lib/python3.5/site-packages/` otteniamo
```sh
total 1852
-rw-r--r-- 1 root staff 1895932 Mar 20 21:51 cv2.cpython-34m.so
```
Ce l'abbiamo fatta. Viceversa qualcosa è andato storto.

**EDIT** Non so quale sia il motivo, ma CMAKE per Python 3 nomina il file `cv2.cpython-35m-arm-linux-gnueabihf.so` che crea una serie indicibile di complicazioni. Per ripristinare il bug basterà rinominare
```sh
$ cd ~/.virtualenvs/cv/lib/python3.5/site-packages/
$ ln -s /usr/local/lib/python3.5/site-packages/cv2.so cv2.so
```

Ci rimane una sola cosa da fare: il test
```sh
$ source ~/.profile 
$ workon cv
$ python3
>>> import cv2
>>> cv2.__version__
'3.3.0'
>>>
```
Ce l'abbiamo fatta. OpenCV è pronto per essere utilizzato :sunglasses:

## 23/10/2018

Abbiamo Python e OpenCV installati correttamente sul nostro pi. Ora vediamo se riusciamo a combinare qualcosa con la nostra PiCamera. Proviamo ad accedere alla nostra camera, e a creare un flusso video su schermo di quello che sta registrando.

Per prima cosa, accediamo in SSH al raspberry (ricordarsi di accedere con `ssh -X pi@pi-address` per poter vedere la finestra streaming proveniente da raspberry). Entriamo nel nostro ambiente virtuale con i comandi

```sh
source ~/.profile
workon cv
```

Nel file test_video.py inseriamo il seguente codice

```Python
# Packages import
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
 
# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
 
# allow the camera to warmup
time.sleep(0.5)
 
# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        # grab the raw NumPy array representing the image
        image = frame.array
 
        # show the frame
        cv2.imshow("Frame", image)
        key = cv2.waitKey(1) & 0xFF
 
        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)
 
        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
                break
```

Con il comando `Python3 test_video.py` dovremmo avere in output una cosa del genere

![test video](docs/pictures/test_video.png)

Nell'immagine Bernardo, la mascotte che ci aiuterà nella nostra avventura.

Bene, ora che abbiamo testato il funzionamento della Picamera, facciamo il passo successivo. La domanda è? riusciamo a capire se qualcuno ha attraversato il nostro campo visivo? La risposta è si (ovviamente), usando i tool che OpenCV ci mette a disposizione.

### Background substraction
Cercherò di spiegare molto brevemente quello che dobbiamo fare è il cosiddetto background substraction. Ci sono molti modi per farlo, alcuni semplici ed altri molto meno intuitivi. I tre metodi principali sono:
+ Gaussian Mixture Model-based foreground utilizzabile attraverso `cv2.BackgroundSubtractorMOG2`
+ Background segmentation utilizzabile attraverso `cv2.BackgroundSubtractorMOG`
+ Bayesian based foreground 

Per chi volesse approfondire consiglio [questo](http://www.ee.surrey.ac.uk/CVSSP/Publications/papers/KaewTraKulPong-AVBS01.pdf), [questo](http://www.zoranz.net/Publications/zivkovicPRL2006.pdf) e [questo](http://goldberg.berkeley.edu/pubs/acc-2012-visual-tracking-final.pdf).

Per farla breve, quello che questi metodi tentano di fare, in maniera diversa, è creare la differenza pixel a pixel tra il background (considerato statico e immutabile) e il foreground, che possiamo definire come tutto quello che differisce dal background. Quello che bisogna fare quindi è acquisire come prima cosa un'immagine il più possibile statica (e preferibilmente a illuminazione controllata) che fungerà da background. Ad ogni passaggio di persona, animale o altro l'algoritmo, facendo la differenza pixel-pixel tra l'immagine statica di background e quella dinamica del frame appena acquisito rileverà il passaggio.

Complichiamo un po' le cose. Creiamo una cartella `test_sorveglianza` con questa gerarchia
```
test_sorveglianza
    |-- hawkeye.py --> programma per il test di sorveglianza
    |-- conf.json  --> json di configurazione
    |-- hawkeye_classes --> cartella contenente le classi utilizzate
        |-- img_temp.py --> classe per la creazione di immagini temporanee
        |-- __init__.py --> init per Python in modo che rilevi la class folder
```

Dentro `hawkeye.py` inseriamo

``` Python
# import the necessary packages
from hawkeye_classes.img_temp import TempImage
from picamera.array import PiRGBArray
from picamera import PiCamera
import argparse
import warnings
import datetime
import dropbox
import imutils
import json
import time
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-c", "--conf", required=True,
	help="path to the JSON configuration file")
args = vars(ap.parse_args())

# filter warnings, load the configuration
warnings.filterwarnings("ignore")
conf = json.load(open(args["conf"]))
client = None

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = tuple(conf["resolution"])
camera.framerate = conf["fps"]
rawCapture = PiRGBArray(camera, size=tuple(conf["resolution"]))

# allow the camera to warmup, then initialize the average frame, last
# uploaded timestamp, and frame motion counter
print("[INFO] warming up...")
time.sleep(conf["camera_warmup_time"])
avg = None
lastUploaded = datetime.datetime.now()
motionCounter = 0

# capture frames from the camera
for f in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# grab the raw NumPy array representing the image and initialize
	# the timestamp and occupied/unoccupied text
	frame = f.array
	timestamp = datetime.datetime.now()
	text = "Unoccupied"

	# resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

	# if the average frame is None, initialize it
	if avg is None:
		print("[INFO] starting background model...")
		avg = gray.copy().astype("float")
		rawCapture.truncate(0)
		continue

	# accumulate the weighted average between the current frame and
	# previous frames, then compute the difference between the current
	# frame and running average
	cv2.accumulateWeighted(gray, avg, 0.5)
	frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))
    	# threshold the delta image, dilate the thresholded image to fill
	# in holes, then find contours on thresholded image
	thresh = cv2.threshold(frameDelta, conf["delta_thresh"], 255,
		cv2.THRESH_BINARY)[1]
	thresh = cv2.dilate(thresh, None, iterations=2)
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]

	# loop over the contours
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < conf["min_area"]:
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
		text = "Occupied"

	# draw the text and timestamp on the frame
	ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
		0.35, (0, 0, 255), 1)
    # check to see if the room is occupied
	if text == "Occupied":
		# check to see if enough time has passed between uploads
		if (timestamp - lastUploaded).seconds >= conf["min_upload_seconds"]:
			# increment the motion counter
			motionCounter += 1

			# check to see if the number of frames with consistent motion is
			# high enough
			if motionCounter >= conf["min_motion_frames"]:
                # write the image to temporary file
                t = TempImage()
                cv2.imwrite(t.path, frame)

				# update the last uploaded timestamp and reset the motion
				# counter
				lastUploaded = timestamp
				motionCounter = 0
	# otherwise, the room is not occupied
	else:
		motionCounter = 0
    	# check to see if the frames should be displayed to screen
	if conf["show_video"]:
		# display the security feed
		cv2.imshow("Security Feed", frame)
		key = cv2.waitKey(1) & 0xFF

		# if the `q` key is pressed, break from the lop
		if key == ord("q"):
			break

	# clear the stream in preparation for the next frame
	rawCapture.truncate(0)
```
la nostra classe `img_temp.py` invece conterrà 

```Python
# import the necessary packages
import uuid
import os

class TempImage:
	def __init__(self, basePath="./", ext=".jpg"):
		# construct the file path
		self.path = "{base_path}/{rand}{ext}".format(base_path=basePath,
			rand=str(uuid.uuid4()), ext=ext)

	def cleanup(self):
		# remove the file
		os.remove(self.path)
```
Dove `basePath` sarà il path dove salvare le immagini quando verrà rilevata un'intrusione, `ext` l'estensione del file immagine desiderata.

Il file `conf.json` invece contiene semplicemente alcune voci con le impostazioni base di caricamento, in modo da non doverle sempre modificare via codice

```json
{
	"show_video": true,
	"min_upload_seconds": 3.0,
	"min_motion_frames": 8,
	"camera_warmup_time": 2.5,
	"delta_thresh": 5,
	"resolution": [640, 480],
	"fps": 16,
	"min_area": 5000
}
```

Ecco il risultato 

![test](/docs/pictures/test_img_sorveglianza.jpg)

Perfetto :smile:

## 26/10/2018

Altro giorno, altro test. Riepilogo di quanto è stato fatto finora:
1. Abbiamo installato OpenCv e i pacchetti correlati sul nostro pi
2. Abbiamo testato il funzionamento di OpenCV con la nostra Pi cam al fine di catturare un movimento nel campo visivo del raspberry

### Google API

Oggi andiamo a testare un'altra parte importante: le Google API per la gestione del gdoc all'interno di drive.

Per prima cosa andiamo sul [google developer portal](https://console.developers.google.com), accediamo e creiamo un nuovo progetto dandogli un nome a nostro piacimento

![google_portal](docs/pictures/google_portal.png)

A questo punto andiamo ad abilitare due api
* Google Sheets API
* Google Drive API

Per farlo, dalla dashboard del progetto appena creato, andiamo in Libreria e li cerchiamo. Una volta trovati basterà cliccare su abilita. Ci verranno fatte un paio di domande sul tipo di applicazione e alla fine ci verrà dato un file .json contenente le nostre credenziali di accesso all'API

**N.B.** questo file permette l'accesso all'account drive google, è quindi fondamnetale salvarlo in un luogo sicuro

A questo punto, se il procedimento è stato fatto dal nostro pc, non ci resterà che spostare il file all'interno del nostro raspberry.

Bene, ora un po' di librerie utili al nostro scopo. Per prima cosa, entriamo nell'ambiente virtuale creato sul nostro pi con i soliti comandi `source ~/.profile` e `workon cv` e creiamo la cartella `test_google_api`.


*Consiglio*: per capire cosa si sta facendo a livello di codice, sarebbe bene consultare la documentazione di [Google API](https://developers.google.com/drive/api/v3/about-sdk) e [gspread](https://github.com/burnash/gspread). Le prime ci permetteranno di accedere ai servizi Google, il secondo pacchetto invece di manipolare i file gdoc.

Prima di procedere, installiamo `gspread` con il comando

```sh
pip install gspread oauth2client
```
**Importante**: il file credentials.json generato dal progetto creato nella Google development portal va inserito nella stessa cartella dov'è presente il codice

### Il codice

A questo punto creiamo il file `spreadsheet.py` all'interno della cartella `test_google_api` creata in precedenza e vi inseriamo

``` Python
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint
from pprint import PrettyPrinter
# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_pi.json', scope)
client = gspread.authorize(creds)

# Find a workbook by url and open the first sheet
# Make sure you use the right url here.
sheet = client.open_by_url('https://docs.google.com/spreadsheets/d/159bdgq15gXq9D173LG1-lp5A6QMv8NnMF31rER1tbmk/edit#gid=1500069035')
# Extract and print all of the values
list_of_hashes = sheet.sheet1.get_all_values()
# Print the result
pp = PrettyPrinter()
pp.pprint(list_of_hashes)

# Search in sheet1 a value and Print the result
cell_list = sheet.sheet1.findall("Christian ")
pp.pprint(cell_list)

# Extract the values of the first column and print the result
col_list = sheet.sheet1.col_values(1)
print(col_list)
```

Ho chiamato il mio file con le credenziali `client_pi.json`. Quello che qui abbiamo fatto è stato 
1. Aprire un file Google sheet utilizzando il suo url, nel mio caso il file con gli Abbonamenti e le anagrafiche del nostro Fablab
2. Estrarre tutti i valori presenti nel foglio 1 e visualizzare il risultato in maniera ordinata usando `pprint` invece del classico print
3. Cercare un valore preciso e stampare il risultato
4. Selezionare tutta la prima Colonna e stampare il risultato

Ecco il mio risultato
![spreadsheet.py](docs/pictures/test_spreadsheet.png)

**EDIT**: Per salvaguardare la privacy, ho tagliato la parte in cui si vedono tutte le informazioni dei nostri abbonati.

Perfetto direi:sunglasses: 

### MFRC522

Bene, non ci resta che testare l'ultima parte importante sul funzionamento del nostro controllo accessi. La lettura del tag RFID  :stuck_out_tongue_closed_eyes:

Come lettore, utilizzeremo il classico MFRC522. Non sto qui a spiegare come funziona la tecnologia Arduino, come sono fatti i tag ecc ma mi limito a lasciare un paio di link molto utili
* [wiki page](https://en.wikipedia.org/wiki/Radio-frequency_identification) è un buon punto di partenza per un'infarinata generica sulla tecnologia
* [Adafruit PN532](https://learn.adafruit.com/adafruit-pn532-rfid-nfc/mifare) lo so, è di un altro dispositivo che non c'entra niente, ma la pagina è ben fatta e da una spiegazione su come sono fatte le mifare card, che utilizzaremo per il progetto
* [datasheet MFCR522](https://www.nxp.com/docs/en/data-sheet/MFRC522.pdf) per i più smanettoni, il datasheet del componente cuore della shield(ebbene si, MFRC522 è un integrato prodotto dalla NXP (aka Philips), non la shield :stuck_out_tongue_winking_eye:)

Nella versione precedente avevo creato una libreria (in linguaggio C) per interfacciare il raspberry con MFRC522. Questa volta, un po' per pigrizia, un po' perchè c'è tantissimo materiale in rete scritto in Python, ho deciso di utilizzare una libreria già fatta.

Dopo aver scartabellato un po' ho trovato [questa](https://github.com/ondryaso/pi-rc522) veramente ben fatta e documentata.

Dopo aver attivato la periferica SPI tramite il comando `sudo raspi-comfig`,aver seguito la documentazione installando le dipendenze necessarie (**n.b** ricordiamoci che tutta la parte precedente di OpenCv è stata installata sul nostro ambiente virtuale quindi anche tutte le dipendenze necessarie per queste andranno installate all'interno dell'ambiente cv) e dopo aver collegato i fili in maniera corretta (ottimo sito per il [pinout](https://pinout.xyz/#) del Raspi), utilizzando il programma di esempio Read.py ho ottenuto il risultato desiderato

![mfrc_test](docs/pictures/mfrc522_test.png)

Verificato il funzionamento, si tratterà di estrapolare le parti necessarie al nostro progetto e inserirle nel nostro programma


### GUI

