# FabGuard

Un semplice controllo accessi ed abbonamenti per il laboratorio

## About the project
La cartella di progetto è suddivisa in varie versioni con **le seguenti regole**:
* **Per il nome delle versioni il progetto si attiene alle regole del [SemVer](http://semver.org/)**
* **La versione stabile del progetto si trova nella cartella denominata vStable. Se non presente, la fase di sviluppo è ancora in beta e la versione più stabile si trova nella cartella con il penultimo numero di sviluppo.** Per esempio, se ci sono le due cartelle v0.2.3 e v0.2.4 la versione stabile è la prima, quella in fase di sviluppo la seconda. 
* **Per Gitlab, la versione stabile è sempre la master**
* **Può essere presente una cartella vArchive in cui sono presenti vecchie versioni ormai non più utilizzate**
Ogni cartella versione contiene le seguenti cartelle:
* collateral: tutto ciò che serve al progetto oltre all'hardware e al software (es. link utili, esempi di libreria ecc)
* firmware: parte software; può contenere diverse sotto-cartelle con con nomi che si attengono alle regole del [SemVer](http://semver.org/). **Per la versione stabile, valgono le regole generali dette sopra**
* hardware: tutti i file necessari per la creazione del pcb; a sua volta contiene le sottocartelle
    * calcoli: fogli di calcolo ecc
    * datasheet: datasheet dei principali componenti utilizzati
    * distinte: distinte materiali per produzione / prototipazione
    * gerber: file gerber pcb
    * pcb: modelli pcb di Vulcano
    * modelli_3d: modelli 3D dell'elettronica
    * sheet: file di Orcad
* mechanic: tutti i file necessari per la creazione di parti meccaniche del progetto
* file utili al progetto ma che non rientrano nelle categorie precedenti (es. immagini, bitmap ecc)
## Getting Started

## Built With

## Versioning

Il progetto si attiene alle regole del [SemVer](http://semver.org/) per le varie versioni software. Per le versioni disponibili, vedere i tag associati al repository, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

Sviluppatori principali:
* Christian Corsetti: 
    * Design case
    * progettazione della parte elettronica
    * firmware