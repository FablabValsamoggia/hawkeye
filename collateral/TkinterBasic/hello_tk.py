import tkinter as tk
from tkinter import ttk

LARGE_FONT = ("Verdana", 16)


class HawkeyTk(tk.Tk):

    def __init__(self, *args, **kwargs):
        
        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.wm_title(self, "Hawkeye")

        container = tk.Frame(self)
        #container.pack(side="top", fill="both", expand = True)
        #container.pack()
        container.place(x = 0, y = 0, width = 1024, height = 600)
        self.frames = {}
        frame = MainPage(container, self)

        """
        for f in (StartPage, PageOne):
            frame = f(container, self)
            self.frames[f] = frame
        """
        self.frames[MainPage] = frame
        #frame.grid(row = 0, column = 0, sticky = "nsew")
        frame.place(x = 0, y = 0, width = 1024, height = 600)
        self.show_frame(MainPage)
    
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

class MainPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        lname = tk.Label(self, text = "Name + Surname", font = LARGE_FONT)
        lname.place(x = 10, y = 10, width = 1000, height = 20)
        #lname.pack(padx = 600, pady = 50)

        lstate = tk.Label(self, text = "Pass State", font = LARGE_FONT)
        #lstate.pack(padx = 10, pady = 50)

        lexpired_date = tk.Label(self, text = "Expired Date", font = LARGE_FONT)
        #lexpired_date.pack(padx = 600, pady = 60)




app = HawkeyTk()
#app.iconbitmap("fab.png")
#app.attributes('-fullscreen', True)
app.geometry("1024x600")
app.mainloop()

"""
        show_pg1_btn = ttk.Button(self, text = "quit", 
                command = parent.quit)
        show_pg1_btn.pack()
"""
    