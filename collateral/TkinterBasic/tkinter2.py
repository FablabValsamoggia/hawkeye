from tkinter import *

root = Tk()

# per default, il main windows pack dal top
topFrame = Frame(root)
topFrame.pack()
# Per un Frame dal bottom invece, devo dirlo esplicitamente
bottomFrame = Frame(root)
bottomFrame.pack(side = BOTTOM)

# Creazione dei Button
# fg colore font testo, bg colore sfondo
button1 = Button(topFrame, text = "Button 1", fg = "red", bg = "yellow")
button2 = Button(topFrame, text = "Button 2", fg = "blue")
button3 = Button(topFrame, text = "Button 3", fg = "green")
button4 = Button(bottomFrame, text = "Button 4", fg = "purple")

# Dispongo i Button creati; se non lo faccio, anche se creati, non appariranno
# a schermo

# pack metti i widget uno sopra l'altro, quindi per disporli in maniera 
# intelligente uso il parametro side. Con LEFT, tkinter disporrà il widget
# il più possibile a sinistra
button1.pack(side = LEFT) # button1, button2, button3 affiancati nel topFrame
button2.pack(side = LEFT)
button3.pack(side = LEFT)
button4.pack(side = BOTTOM)

root.mainloop()