from tkinter import *

root = Tk()

def printName():
    print("My name is Christian!")


def eventFunction(event):
    print("My name is pippo!")
# Creo i widget
# binding a function to a widget con il parametro command; nel chiamare la 
# funzione non servono parentesi
#button1 = Button(root, text = "print my name", command = printName)
button1 = Button(root, text = "print my name")
# bind associa al button1 l'evento click con il mouse sinistro
button1.bind("<Button-1>", eventFunction)
button1.pack()

root.mainloop()