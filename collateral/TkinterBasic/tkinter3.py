from tkinter import *

root = Tk()

# Creo i widget
lOne = Label(root, text = "One", bg = "red", fg = "white")
lTwo = Label(root, text = "Two", bg = "green", fg = "black")
lThree = Label(root, text = "Three", bg = "yellow", fg = "black")
# Dispongo i widget
lOne.pack()
# con fill dico a pack di riempire tutto lo spazio; quindi con fill = x, andrò
# a riempire tutta la lunghezza in x della finestra anche nei resize
lTwo.pack(fill = X)
lThree.pack(side = LEFT, fill = Y)

root.mainloop()