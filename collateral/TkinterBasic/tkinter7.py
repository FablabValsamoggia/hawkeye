from tkinter import *


class MyButtons:
    #master sta per main windows, in questo caso root
    def __init__(self, master):
        frame = Frame(master)
        frame.pack()

        self.printButton = Button(frame, text = "print message", command = self.printMessage)
        self.printButton.pack(side = LEFT)

        self.quickButton = Button(frame, text = "quit", command = frame.quit)
        self.quickButton.pack(side = LEFT)
    
    def printMessage(self):
        print("hello, this work")

root = Tk()
b = MyButtons(root)
root.mainloop()