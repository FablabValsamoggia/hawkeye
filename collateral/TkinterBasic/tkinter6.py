from tkinter import *

root = Tk()

def leftClick(event):
    print("left mouse click")

def middleClick(event):
    print("middle mouse click")

def rightClick(event):
    print("right mouse click")

frame = Frame(root, width = 300, height = 300)
frame.bind("<Button-1>", leftClick)
frame.bind("<Button-2>", middleClick)
frame.bind("<Button-3>", rightClick)

frame.pack()

root.mainloop()