from  tkinter import *

root = Tk()
root.configure(bg = "white")
# Creo i widget
lUser = Label(root, text = "Name", bg = "white")
lPwd  = Label(root, text = "Password", bg = "white")
# Entry per input testo
eUser = Entry(root, bg = "white")
ePwd = Entry(root, bg = "white")
# Checkbox
chk = Checkbutton(root, text = "Keep me logged in", bg = "white")

# Dispongo i widget sulla finestra con grid; sticky permette di allineare,
#all'interno della cella, dove posizionare il widget. valori sono N, S, E, W
lUser.grid(row = 0, column = 0, sticky = E)
lPwd.grid(row = 1, column = 0, sticky = E)
eUser.grid(row = 0, column = 1)
ePwd.grid(row = 1, column = 1)
# columnspan indica di usare più colonne per il widget; in questo caso voglio 
# tutte e due le colonne per la checkbox. Il check box verrà posizionato al 
# centro delle due colonne
chk.grid(columnspan = 2)
root.mainloop()