from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from apiclient.http import MediaIoBaseDownload
import io

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/drive'

def main():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('drive', 'v3', http=creds.authorize(Http()))
    
    # Call the Drive v3 API
    results = service.files().list(pageSize=30, fields="nextPageToken, files(id, name)").execute()    
    items = results.get('files', [])
    page_token = None

    while True:
        response = service.files().list(q="name='Anagrafica + Abbonamenti'", 
                                            spaces='drive',
                                            fields='nextPageToken, files(id, name)',
                                            pageToken=page_token).execute()
        #print(response)
        # verifico se il file esiste
        if not response['files']:
            print('no file found')
            break
        
        for f in response.get('files', []):
            # Process change
            print ('Found file: %s (%s)' % (f.get('name'), f.get('id')))
        
        file_id = f.get('id')
        request = service.files().export_media(fileId=file_id,
                                             mimeType='application/x-vnd.oasis.opendocument.spreadsheet')
        #fh = io.BytesIO()
        fh = io.FileIO(f.get('name'), 'wb')
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print ("Download %d%%." % int(status.progress() * 100))
        #return(fh.getvalue())

        page_token = response.get('nextPageToken', None)
        if page_token is None: 
            break
    
#    if not items:
#        print('No files found.')
#    else:
#        print('Files:')
#        for item in items:
#            print(u'{0} ({1})'.format(item['name'], item['id']))

if __name__ == '__main__':
    main()

'''
    page_token = None
    while True:
        response = drive_service.files().list(q="name='Anagrafica + Abbonamenti'",
                                            spaces='drive',
                                            fields='nextPageToken, files(id, name)',
                                            pageToken=page_token).execute()
        for file in response.get('files', []):
            # Process change
            print ('Found file: %s (%s)' % (file.get('name'), file.get('id')))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
'''