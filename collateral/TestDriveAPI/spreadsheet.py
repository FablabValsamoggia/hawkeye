import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint
from pprint import PrettyPrinter
# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scope)
client = gspread.authorize(creds)

# Find a workbook by name and open the first sheet
# Make sure you use the right name here.
#sheet = client.open("controllo_accessi/AnagraficaTestNoGood").sheet1
sheet = client.open_by_url('https://docs.google.com/spreadsheets/d/159bdgq15gXq9D173LG1-lp5A6QMv8NnMF31rER1tbmk/edit#gid=1511069035')
# Extract and print all of the values
#list_of_hashes = sheet.get_all_records()
pp = PrettyPrinter()
#list_of_hashes = sheet.sheet1.get_all_values()
#pp.pprint(list_of_hashes)

cell_list = sheet.sheet1.findall("Flavio")
pp.pprint(cell_list)

col_list = sheet.sheet1.col_values(1)
print(col_list)
