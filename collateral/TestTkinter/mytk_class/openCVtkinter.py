# import the necessary packages
from __future__ import print_function
from PIL import Image
from PIL import ImageTk
import tkinter as tki
import tkinter as GUI
import threading
import datetime
import imutils
import cv2
import os
import json

class PhotoBoothApp:
	def __init__(self, vs, outputPath, config):
		# store the video stream object and output path, then initialize
		# the most recently read frame, thread for reading frames, and
		# the thread stop event
		self.vs = vs
		self.config = json.load(open(config))
		self.outputPath = outputPath
		self.frame = None
		self.thread = None
		self.stopEvent = None
		# initialize the root window and image panel
		self.root = tki.Tk()
		self.panel = None
		#self.root.attributes('-fullscreen', True)
		# create a button, that when pressed, will take the current
		# frame and save it to file
		btn = tki.Button(self.root, text="Snapshot!", command=self.takeSnapshot, height=10, width=10)
		btn.config(state=GUI.DISABLED)
		
		#btn.place(x=800, y=10)
			
		#btn.pack(side="bottom", fill="both", expand="yes", padx=10, pady=10)
		btn.pack(side="right", expand="no", pady=10)
		# start a thread that constantly pools the video sensor for
		# the most recently read frame
		self.stopEvent = threading.Event()
		self.thread = threading.Thread(target=self.videoLoop, args=())
		self.thread.start()

		# set a callback to handle when the window is closed
		self.root.wm_title("HawkeyPI")
		self.root.wm_protocol("WM_DELETE_WINDOW", self.onClose)
		#self.root.wm_protocol("q", self.onClose)

	def videoLoop(self):
		# keep looping over frames until we are instructed to stop
		# initialize the first frame in the video stream
		firstFrame = None
		lastUploaded = datetime.datetime.now()
		motionCounter = 0
		while not self.stopEvent.is_set():
			# grab the frame from the video stream and resize it to
			# have a maximum width of 300 pixels
			self.frame = self.vs.read()
			text = "Unoccupied"
			#self.frame = imutils.resize(self.frame, width=500)
			self.frame = imutils.resize(self.frame, 300)
			gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
			gray = cv2.GaussianBlur(gray, (21, 21), 0)
			
			if firstFrame is None:
				firstFrame = gray
				print("set first frame")
				continue
			
			# compute the absolute difference between the current frame and
			# first frame
			frameDelta = cv2.absdiff(firstFrame, gray)
			thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

			# dilate the thresholded image to fill in holes, then find contours
			# on thresholded image
			thresh = cv2.dilate(thresh, None, iterations=2)
			cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
				cv2.CHAIN_APPROX_SIMPLE)
			cnts = cnts[0] if imutils.is_cv2() else cnts[1]

			# loop over the contours
			for c in cnts:
				# if the contour is too small, ignore it
				if cv2.contourArea(c) < self.config["min_area"]: #args["min_area"]:
					continue

				# compute the bounding box for the contour, draw it on the frame,
				# and update the text
				(x, y, w, h) = cv2.boundingRect(c)
				cv2.rectangle(self.frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
				text = "Occupied"

			# draw the text and timestamp on the frame
			cv2.putText(self.frame, "Room Status: {}".format(text), (10, 20),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
			cv2.putText(self.frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
				(10, self.frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)			
			# OpenCV represents images in BGR order; however PIL
			# represents images in RGB order, so we need to swap
			# the channels, then convert to PIL and ImageTk format
			image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
			image = Image.fromarray(image)
			image = ImageTk.PhotoImage(image)
			# if the panel is not None, we need to initialize it
			if self.panel is None:
				self.panel = tki.Label(image=image)
				self.panel.image = image
				#self.panel.grid(row=0, column=0)
				#self.panel.pack(side="left", padx=10, pady=10)
				self.panel.place(x= 0, y = 0)
			# otherwise, simply update the panel
			else:
				self.panel.configure(image=image)
				self.panel.image = image
				

	def takeSnapshot(self):
		# grab the current timestamp and use it to construct the
		# output path
		ts = datetime.datetime.now()
		filename = "{}.jpg".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))
		p = os.path.sep.join((self.outputPath, filename))

		# save the file
		cv2.imwrite(p, self.frame.copy())
		print("[INFO] saved {}".format(filename))

	def onClose(self):
		# set the stop event, cleanup the camera, and allow the rest of
		# the quit process to continue
		print("[INFO] closing...")
		self.stopEvent.set()
		self.vs.stop()
		self.root.quit()