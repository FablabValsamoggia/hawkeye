from tkinter import *

LARGE_FONT = ("Verdana", 16)
GIGA_FONT = ("Monospace", 20)

class AdminPage:
    
    def __init__(self, master):
        self.master = master
        self.frame = Frame(self.master)
        self.label1 = Label(self.master, text = "admin page")
        self.label1.place(x = 10, y = 10)

class MainPage(Frame):
    def __init__(self, master):
        # Create Frame for video widget and control widget like button
        # and label
        videoFrame = Frame(master, width = 400, 
                            height = 480, bg = "blue")
        controlFrame = Frame(master, width = 400, 
                            height = 480, bg = "white")

        # Place the Frames
        videoFrame.place(x = 0, y = 0)
        controlFrame.place(x = 401, y = 0)
        
        # Add Fablab Logo on controlFrame
        self.photo = PhotoImage(file = "fab.png")
        self.l_logo = Label(root, image = self.photo, bg = "white")
        
        # Add the labels
        self.l_name       = Label(controlFrame, text = "NOME + COGNOME", 
                                    bg = "white", font = LARGE_FONT)
                        
        self.l_pass_state = Label(controlFrame, text = "Pass State", 
                                    bg = "white", font = LARGE_FONT)
                        
        self.l_expired    = Label(controlFrame, text = "Expired Date", 
                                    bg = "white", font = LARGE_FONT)
        # Add the buttons
        self.btn_new_member = Button(controlFrame, text = "New User", 
                                        bg = "white", font = GIGA_FONT, 
                                        command = self.OnClick)
        self.btn_admin      = Button(controlFrame, text = "Admin", bg = "white", 
                                    font = GIGA_FONT)
        # Config the buttons size
        self.btn_new_member.config(width = 10, height = 2)
        self.btn_admin.config(width = 10, height = 2)

        # Place the widgets on controlFrame in the right postion
        self.l_name.place(relx = 0.5, rely = 0.3, anchor = CENTER)

        self.l_pass_state.place(relx = 0.5, rely = 0.5, anchor = CENTER)
    
        self.l_expired.place(relx = 0.5, rely = 0.7, anchor = CENTER)

        self.l_logo.place(x = 500, y = 10, anchor = NW)

        self.btn_new_member.place(relx = 0.5, rely = 0.9, 
                            bordermode = OUTSIDE, anchor = W)
        self.btn_admin.place(relx = 0.02, rely = 0.9,
                            bordermode = OUTSIDE, anchor = W)
    
    def OnClick(self):
        self.l_name.config(text = "Premuto")


root = Tk()
MainWindow = MainPage(root)
root.geometry("800x480")
root.resizable(width = FALSE, height = FALSE)
root.title("Hawkeye")
root.mainloop()
