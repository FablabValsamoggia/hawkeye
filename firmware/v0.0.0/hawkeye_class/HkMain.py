from __future__ import print_function
import tkinter as tk
from PIL import Image
from PIL import ImageTk
from imutils.video import VideoStream
import datetime
import imutils
import cv2
import os
import json
import time
# personal class
from hawkeye_class.keyboard import Keyboard
from hawkeye_class.HawkeyPass import Hawkey522
from hawkeye_class.HkAdmin import AdminPage
from hawkeye_class.HkMember import MemberPage
from hawkeye_class.HawkeyPass import RFIDThread

NORMAL_FONT= ("Verdana", 15)
LARGE_FONT= ("Verdana", 18)
GIGA_FONT = ("Monospace", 20)

class MainPage(tk.Frame):
    """ 
    Main windows of the Program: in this class there are the video from 
    Pi Camera, the buttons for admin settings and member settings and other
    cool stuff
    """
    def __init__(self, parent, controller, jconf):
        tk.Frame.__init__(self,parent)
        # Create Frame for video widget and control widget like button
        # and label
        videoFrame   = tk.Frame(self, width = 400, height = 480, bg = "white")
        controlFrame = tk.Frame(self, width = 400, height = 480, bg = "white")

        # Place the Frames
        videoFrame.place(x = 0, y = 0)
        controlFrame.place(x = 400, y = 0)

        # Add Fablab Logo on controlFrame
        self.photo  = tk.PhotoImage(file = "/home/morpheus/PRJ2018/Fablab/control_access/v1.0.0/firmware/v0.0.0/hawkeye_class/fab.png")
        self.l_logo = tk.Label(self, image = self.photo, bg = "white")
        
        # Add the labels
        self.l_name       = tk.Label(controlFrame, text = "NOME + COGNOME", 
                                        bg = "white", font = LARGE_FONT)
        self.l_pass_state = tk.Label(controlFrame, text = "Pass State", 
                                        bg = "white", font = LARGE_FONT)                        
        self.l_expired    = tk.Label(controlFrame, text = "Expired Date", 
                                        bg = "white", font = LARGE_FONT)
        self.l_welcome = tk.Label(videoFrame, text = "",
                                        bg = "white", font = GIGA_FONT)
        self.panel = tk.Label(videoFrame, bg = "white")

        # Add the buttons
        self.btn_member = tk.Button(controlFrame, text = "New User", 
                                        bg = "white", font = GIGA_FONT, 
                                        command=lambda: controller.show_frame(MemberPage))
        self.btn_admin  = tk.Button(controlFrame, text = "Admin", 
                                        bg = "white", font = GIGA_FONT,
                                        command = lambda:controller.show_frame(AdminPage))
        self.btn_in     = tk.Button(videoFrame, text = "IN", bg = "green", 
                                        font = GIGA_FONT, state = tk.DISABLED)
        self.btn_out    = tk.Button(videoFrame, text = "OUT", bg = "white",
                                        font = GIGA_FONT, state = tk.DISABLED)
        
        # Config the buttons size
        self.btn_member.config(width = 10, height = 2)
        self.btn_admin.config(width = 10, height = 2)
        self.btn_in.config(width = 8, height = 2)
        self.btn_out.config(width = 8, height = 2)

        # Place the widgets on controlFrame in the right postion
        self.l_logo.place(x = 500, y = 10, anchor = tk.NW)
        self.l_name.place(relx = 0.5, rely = 0.3, anchor = tk.CENTER)
        self.l_pass_state.place(relx = 0.5, rely = 0.5, anchor = tk.CENTER)
        self.l_expired.place(relx = 0.5, rely = 0.7, anchor = tk.CENTER)
        self.l_welcome.place(x = 200, y = 336, anchor = tk.CENTER)
        self.panel.place(x = 0, y = 0)

        self.btn_member.place(relx = 0.5, rely = 0.9, bordermode = tk.OUTSIDE, 
                                anchor = tk.W)
        self.btn_admin.place(relx = 0.02, rely = 0.9, bordermode = tk.OUTSIDE, 
                                anchor = tk.W)
        self.btn_in.place(relx = 0.08, rely = 0.9, bordermode = tk.OUTSIDE,
                                anchor = tk.W)
        self.btn_out.place(relx = 0.55, rely = 0.9, bordermode = tk.OUTSIDE,
                                anchor = tk.W)
        # set some variables for the videoLoop
        self.firstFrame = None
        self.text = "Unocuppied"
        self.id = 0
        self.motion = 0
        self.last_detect = datetime.datetime.now()
        self.check = Hawkey522(5, jconf["opening"])
        self.pi_cam = jconf["picam"]
        self.motionCounter = jconf["min_motion_frames"]
        self.outpath = jconf["outpath"]
        self.min_area = jconf["min_area"]
        self.detect_time = jconf["last_detect_time"]
        self.rfid_tag = RFIDThread(5)
        self.rfid_tag.start()
        self.vs = VideoStream(usePiCamera=self.pi_cam > 0).start()
        print("Warming the camera ..")
        time.sleep(jconf["camera_warmup_time"])
        #print(controller.conf)
        self.videoLoop()
        

    def videoLoop(self):
        """
        Video capture from camera and check the intrusion with cv2 
        If an intrusion is detected, call the functions for:
            - read a RFID MiFARE classic card
            - Save a picture of the walk-in
        """

        # grab the frame from the video stream and resize it to
        # have a maximum width of 380 pixels
        self.frame = self.vs.read()
        self.frame = imutils.resize(self.frame, width = 380)
        # Convert the frame in grayscale, and blur it
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)
        # get the current time
        timestamp = datetime.datetime.now()
        # Initialize the first frame. The algorithm compare this frame with the
        # others (pixel to pixel)
        if self.firstFrame is None:
            self.firstFrame = gray
            print("set first frame")            
        else:
            # compute the absolute difference between the current frame and
			# first frame
            frameDelta = cv2.absdiff(self.firstFrame, gray)
            thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
            # dilate the thresholded image to fill in holes, then find contours
			# on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)
            cnts = cnts[0] if imutils.is_cv2() else cnts[1]
            
            # loop over the contours
            for c in cnts:
                # if the contour is too small, ignore it
                if cv2.contourArea(c) < self.min_area:
                    continue
                # compute the bounding box for the contour, draw it on the frame,
				# and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(self.frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                self.text = "Occupied"
                # Set the welcome label and call the function for read the tag
                if self.motion == 0:
                    print(int(time.time()))
                    #self.check.start_time(int(time.time()))
                    self.l_welcome.configure(text = "Ciao!!! Ci conosciamo?", 
                                                font = GIGA_FONT, fg = "black")
                    self.motion = 1
            
            # draw the text and timestamp on the frame
            cv2.putText(self.frame, "Room Status: {}".format(self.text), (10, 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.putText(self.frame, 
                datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, self.frame.shape[0] - 10), 
                cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
            # OpenCV represents images in BGR order; however PIL
            # represents images in RGB order, so we need to swap
            # the channels, then convert to PIL and ImageTk format
            image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(image)
            image = ImageTk.PhotoImage(image)

            # if the panel is not None, we need to initialize it
            if self.panel is None:
                self.panel = tk.Label(image=image)#tki.Label(image=image)
                self.panel.image = image
                #self.panel.pack(side="left", padx=10, pady=10)
                #self.panel.place(x = 0, y = 0)
            # otherwise, simply update the panel
            else:
                self.panel.configure(image=image)
                self.panel.image = image

        if self.text == "Occupied":
            if (timestamp - self.last_detect).seconds >= self.detect_time:
                
                #ret = self.check.check_pass()
                self.l_welcome.configure(text = "Non è educato non presentarsi sai?",
                                                font = NORMAL_FONT,
                                                fg = "red")
                #elif ret == 0:
  
                self.last_detect = timestamp           
                self.motion += 1
                if self.motion > self.motionCounter:
                    ts = datetime.datetime.now()
                    filename = "{}.jpg".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))
                    p = os.path.sep.join((self.outpath, filename))
                    cv2.imwrite(p, self.frame.copy())
                    print("[INFO] saved {}".format(filename))
                    self.motion = 0

                    #self.panel.after_cancel(self.id)
                print("valore rdr {0}".format(self.rfid_tag.rdr))
                self.rfid_tag.cancel()
        
        self.id = self.panel.after(50, self.videoLoop)
        # per stoppare il timer usare
        # self.panel.after_cancel(self.id)
        self.text = "Unoccupied"

