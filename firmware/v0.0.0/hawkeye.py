# import the necessary packages
import tkinter as tk
from hawkeye_class.HawkeyeTkClass import HawkeyeTkClass
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-c", "--conf", required=True,
	help="path to the JSON configuration file")
args = vars(ap.parse_args())

root = HawkeyeTkClass(args["conf"])
root.geometry("800x480")
root.resizable(width = tk.FALSE, height = tk.FALSE)
root.title("Hawkeye")
img = tk.Image("photo", file="~/PRJ2018/Fablab/control_access/v1.0.0/firmware/v0.0.0/hawkeye_class/fab-2.gif")
root.tk.call('wm', 'iconphoto',root._w, img)
root.mainloop()

