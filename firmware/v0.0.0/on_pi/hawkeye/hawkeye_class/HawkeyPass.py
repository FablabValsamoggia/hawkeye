import tkinter as tk
from threading import Thread, Timer, Event
import time
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint
from pprint import PrettyPrinter
import signal
import time
import sys
"""
from pirc522 import RFID

class RFIDThread():        
    rdr = 0
    tag = 0             
    util = None            

    def __init__(self, t): 
        self.t = t          
        self.thread = Timer(self.t, self.handle_function)
        self.rdr = RFID()
        self.util = self.rdr.util()
        self.util.debug = True
        signal.signal(signal.SIGINT, self.end_read)

    def handle_function(self):
        #self.test_thread() 
        self.mfrc522()
        self.thread = Timer(self.t, self.handle_function)
        self.thread.start()

    def start(self):       
        self.thread.start()

    def cancel(self):      
        self.thread.cancel()
        print("thread cancellato")

    def test_thread(self): 
        self.rdr += 1
        print("test thread")
    
    def mfrc522(self):
        print("cerco un tag")
        self.rdr.wait_for_tag()
        print("fine wait tag")
        (error, uid) = self.rdr.request()
        if not error:
            print("tag trovato uid {0}".format(uid))
            self.tag = True
    
    def end_read(self, signal,frame):
        self.rdr.cleanup()
        sys.exit()


"""
class Hawkey522():

    open_days = {}
    client = None
    pp = None

    #run = True
    rdr = None
    util = None

    def __init__(self, sec, days):
        self.sec = sec
        self.start = 0
        self.elapsed = 0
        self.open_days = days
        #signal.signal(signal.SIGINT, end_read)
        # use creds to create a client to interact with the Google Drive API
        """
        scope = ['https://spreadsheets.google.com/feeds',
                'https://www.googleapis.com/auth/drive']
        creds = ServiceAccountCredentials.from_json_keyfile_name('client_pi.json', scope)
        self.client = gspread.authorize(creds)
        self.pp = PrettyPrinter()
"""
    def start_time(self, time_start):
        self.start = time_start
    
    def check_pass(self):
        if self.elapsed < self.sec:
            print(self.elapsed)
            #sheet = self.client.open_by_url('https://docs.google.com/spreadsheets/d/159bdgq15gXq9D173LG1-lp5A6QMv8NnMF31rER1tbmk/edit#gid=1511069035')
            self.elapsed = int(time.time()) - self.start
            #cell_list = sheet.sheet1.findall("Fiorenzo")
            #list_of_hashes = sheet.sheet1.get_all_values()
            #self.pp.pprint(list_of_hashes)
            #time.sleep(1)
            # qui l'attesa per il tag; return 0 se il timer non è ancora finito
            # return 1 se ho trovato il tag, return 100 se non ho trovato
            return 0
        else:
            return 100
