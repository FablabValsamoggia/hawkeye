################################################################################
#
#
#
################################################################################

from __future__ import print_function
import tkinter as tk
from PIL import Image
from PIL import ImageTk
from imutils.video import VideoStream
import datetime
import imutils
import cv2
import os
import json
import time
# personal class
from hawkeye_class.keyboard import Keyboard
#from hawkeye_class.HawkeyPass import Hawkey522
from hawkeye_class.HkMain import MainPage
from hawkeye_class.HkAdmin import AdminPage
from hawkeye_class.HkMember import MemberPage

NORMAL_FONT= ("Verdana", 15)
LARGE_FONT= ("Verdana", 18)
GIGA_FONT = ("Monospace", 20)
# TODO: sistemare variabili istanza e variabili classe
class HawkeyeTkClass(tk.Tk):
    """
    Super Class for the GUI; init the Frames and some other stuff
    """
    def __init__(self, cnf):      
        tk.Tk.__init__(self)
        
        #self.attributes('-fullscreen', True)
        # create the container
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        
        # open json file config
        d_config = json.load(open(cnf))
        
        # Dictionary for multi page in frames; for more frame, insert into 
        # the for cycle
        self.frames = {}
        for F in (MainPage, AdminPage, MemberPage):
            # Add the frame and set rows and columns
            frame = F(container, self, d_config)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        # Diplay the Main frame
        self.show_frame(MainPage)
        
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()
    
    def set_frame(self, num):
        # TODO: there is a better implementation??
        if num == 0:
            frame = self.frames[MainPage]
        elif num == 1:
            frame = self.frames[AdminPage]
        elif num == 2:
            frame = self.frames[MemberPage]
        else:
            pass
        frame = frame.tkraise()
    

if __name__=='__main__':
    root = HawkeyeTkClass()
    root.geometry("800x480")
    root.resizable(width = tk.FALSE, height = tk.FALSE)
    root.title("Hawkeye")
    root.mainloop()
