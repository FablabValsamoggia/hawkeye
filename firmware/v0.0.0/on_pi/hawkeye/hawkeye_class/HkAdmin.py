from __future__ import print_function
import tkinter as tk


from hawkeye_class.keyboard import Keyboard
#import hawkeye_class.HawkeyPass as p

NORMAL_FONT= ("Verdana", 15)
LARGE_FONT= ("Verdana", 18)
GIGA_FONT = ("Monospace", 20)

class AdminPage(tk.Frame):

    def __init__(self, parent, controller, jconf):
        
        tk.Frame.__init__(self, parent)
        self.config(bg = "white")
        # Add Fablab Logo on controlFrame
        self.myphoto  = tk.PhotoImage(file = "~/hawkeye/hawkeye_class/fab.png")
        l_llogo = tk.Label(self, image = self.myphoto, bg = "white")
        
        btn_rn       = tk.Button(self, text = "Renews", bg = "white", 
                                        font = GIGA_FONT)
        btn_alarm    = tk.Button(self, text = "Disable Alarm", bg = "white",
                                        font = GIGA_FONT)
        btn_notify   = tk.Button(self, text = "Disable Email", bg = "white",
                                        font = GIGA_FONT)
        btn_settings = tk.Button(self, text = "Settings", bg = "white",
                                        font = GIGA_FONT)
        btn_quit     = tk.Button(self, text = "Quit", bg = "#e60012",
                                        font = GIGA_FONT,
                                        command = lambda : self._root().quit())
        btn_back     = tk.Button(self, text = "Back Home", bg = "green",
                                        font = GIGA_FONT,
                                        command = lambda : controller.set_frame(0))
        
        # Config the buttons size
        btn_rn.config(width = 14, height = 2)
        btn_alarm.config(width = 14, height = 2)
        btn_notify.config(width = 14, height = 2)
        btn_settings.config(width = 14, height = 2)
        btn_quit.config(width = 10, height = 2)
        btn_back.config(width = 10, height = 2)

        # Place the widgets on controlFrame in the right postion
        l_llogo.place(x = 500, y = 10, anchor = tk.NW)
        btn_rn.place(relx = 0.5, rely = 0.3, anchor = tk.CENTER)
        btn_alarm.place(relx = 0.5, rely = 0.5, anchor = tk.CENTER)
        btn_notify.place(relx = 0.5, rely = 0.7, anchor = tk.CENTER)
        btn_settings.place(relx = 0.5, rely = 0.9, anchor = tk.CENTER)
        btn_quit.place(relx = 0.75, rely = 0.9, anchor = tk.W)
        btn_back.place(relx = 0.01, rely = 0.9, anchor = tk.W)
