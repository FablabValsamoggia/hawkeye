from __future__ import print_function
import tkinter as tk


from hawkeye_class.keyboard import Keyboard
#from hawkeye_class.HawkeyPass import Hawkey522

NORMAL_FONT= ("Verdana", 15)
LARGE_FONT= ("Verdana", 18)
GIGA_FONT = ("Monospace", 20)


class MemberPage(tk.Frame):

    def __init__(self, parent, controller, jconf):
        tk.Frame.__init__(self, parent)
        self.config(bg = "white")
        
        # Add Fablab Logo on controlFrame
        KeyboardFrame = tk.Frame(self, width = 800, height = 250, bg = "white")

        self.photo  = tk.PhotoImage(file = "~/hawkeye/hawkeye_class/fab.png")
        l_logo = tk.Label(self, image = self.photo, bg = "white")
        l_name = tk.Label(self, text = "Name", font = LARGE_FONT, bg = "white")
        l_surname = tk.Label(self, text = "Surname", font = LARGE_FONT, bg = "white")
        l_email = tk.Label(self, text = "E-mail", font = LARGE_FONT, bg = "white")
        
        e_name = tk.Entry(self, width = 18, font = LARGE_FONT, bg = "white")
        e_surname = tk.Entry(self, width = 18, font = LARGE_FONT, bg = "white")
        e_mail = tk.Entry(self, width = 18, font = LARGE_FONT, bg = "white")

        v = tk.IntVar()
        r_pass_3 = tk.Radiobutton(self, text = "3 Months", bg = "white", 
                                        font = LARGE_FONT, variable = v, 
                                        value = 3, indicatoron = tk.FALSE)
        r_pass_6 = tk.Radiobutton(self, text = "6 Months", bg = "white", 
                                        font = LARGE_FONT, variable = v, 
                                        value = 6, indicatoron = tk.FALSE)
        r_pass_1y = tk.Radiobutton(self, text = "One Year", bg = "white", 
                                        font = LARGE_FONT, variable = v, 
                                        value = 12, indicatoron = tk.FALSE)
        r_pass_cd = tk.Radiobutton(self, text = "Only Card", bg = "white", 
                                        font = LARGE_FONT, variable = v, 
                                        value = 0, indicatoron = tk.FALSE)

        btn_tag      = tk.Button(self, text = "Tag", bg = "white",
                                        font = GIGA_FONT)
        btn_back     = tk.Button(self, text = "Back Home", bg = "green",
                                        font = GIGA_FONT,
                                        command = lambda : controller.set_frame(0))                

        r_pass_3.configure(width = 8, height = 2)
        r_pass_6.configure(width = 8, height = 2)
        r_pass_1y.configure(width = 8, height = 2)
        r_pass_cd.configure(width = 8, height = 2)
        btn_tag.config(width = 10, height = 2)
        btn_back.config(width = 10, height = 2)
        # Place the widgets on controlFrame in the right postion
        KeyboardFrame.place(x = 0, y = 250)
        k = Keyboard(KeyboardFrame)
        k.place(x = 50, y = 0, anchor = tk.NW)
        k.config(bg = "white")

        l_logo.place(x = 500, y = 10, anchor = tk.NW)

        l_name.place(x = 10, y = 30, anchor = tk.W)
        l_surname.place(x = 10, y = 90, anchor = tk.W)
        l_email.place(x = 10, y = 150, anchor = tk.W)

        e_name.place(x = 200, y = 30, anchor = tk.W)
        e_surname.place(x = 200, y = 90, anchor = tk.W)
        e_mail.place(x = 200, y = 150, anchor = tk.W)

        r_pass_3.place(x = 10, y = 220, anchor = tk.W)
        r_pass_6.place(x = 140, y = 220, anchor = tk.W)
        r_pass_1y.place(x = 270, y = 220, anchor = tk.W)
        r_pass_cd.place(x = 400, y = 220, anchor = tk.W)

        btn_tag.place(x = 600, y = 100, anchor = tk.NW)
        btn_back.place(x = 600, y = 180, anchor = tk.NW)
